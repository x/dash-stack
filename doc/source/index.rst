Welcome to dash-stack's documentation!
==========================================

Installation
============

Follow this part of the document to install dash-stack in your environment.

Requirements
------------

In order dash-stack to work correctly, you must install following OS packages:
- MariaDB Server 10.1
- python-virtualenv
- python-pip
- libmariadbclient-dev
- libmysqlclient18-dev
- openssl
- libssl-dev
- gettext
- apache2
- libapache2-mod-wsgi

How to Install
--------------


Configuration
=============

Developer
=========

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
